#include <iostream>
#include <string>
#include <vector>

using namespace std;
// Lets try out vector handling

int main() {
    std::vector<string> vec = {"abc", "def", "ghi", "jkl", "mno", "pqr"};
    // Report vec content
    cout << "vec (" << vec.size() << " items): ";
    for (int i = 0; i<vec.size(); i++) {
        cout << vec[i] << " ";
    }
    cout << endl;

    // Now remove the third item
    vec.erase(vec.begin()+2);

    // Report vec content
    cout << "vec (" << vec.size() << " items): ";
    for (int i = 0; i<vec.size(); i++) {
        cout << vec[i] << " ";
    }
    cout << endl;

}
