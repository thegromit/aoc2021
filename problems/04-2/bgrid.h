/*
// Class BGrid - 5x5 Bingo grid of BCell instances
// 
// Created for the Advent Of Code 2021, day 4 problem 1
// Day 4: Giant Squid
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-09
*/

#include <iostream>

using namespace std;

#include "bcell.h"

class BGrid  {
    public:
        // Containers
        BGrid();
        BGrid(istream& input);
        // Marking
        bool checkAndMark(int num);                         // Check a number and tell if there is a hit
        // Bingo?
        bool isBingo();                                     // Five in a row or column
        // Cell access// Cell access
        BCell& getCell(int x, int y);
        int getValue(int x, int y);
        void setCell(int x, int y, BCell cell);
        void setValue(int x, int y, int val);
        int getBingoRow();                                  // Row of the achieved Bingo or -1 if no row has bingo
        int getBingoCol();                                  // Column of the achieved Bingo or -1 if no Column has bingo

    private:
        // Private fields here
        int bingoRow, bingoCol;
        BCell theGrid[5][5];                                // 5x5 grid of BCells
};
