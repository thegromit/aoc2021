/*
// Class BCell - Cell in a 5x5 Bingo grid
// 
// Created for the Advent Of Code 2021, day 4 problem 1
// Day 4: Giant Squid
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-09
*/


class BCell {
    public:
        // Constructors
        BCell();
        BCell(int value);
        // About value
        void setValue(int value);
        int getValue();
        // About being hit
        bool isHit();
        void markHit();
        void clearHit();
    private:
        int theValue;
        bool hitValue;
};
