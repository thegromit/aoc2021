// Solution at the Advent Of Code 2021, day #01 problem 1
// Day 1: Sonar Sweep
// https://adventofcode.com
//
// Erkki A. Pöyhönen, 2021-12-29

#include <iostream>
#include <string>

using namespace std;

int main() {
    int depth=-1, prev_depth = -1, increased_times = 0;
    cin >> depth;
    while (cin) {
        cout << depth;
        if (prev_depth != -1) {     // Not the 1st measurement
            if (depth > prev_depth) {
                cout << " (increased)";
                increased_times++;
            }
            else if (depth < prev_depth)
                cout << " (decreased)";
        }
        cout << endl;
        prev_depth = depth;
        cin >> depth;
    }
    cout 
        << "==========================" << endl 
        << "Depth increased " << increased_times << " times." << endl;
}