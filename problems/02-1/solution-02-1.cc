// Solution at the Advent Of Code 2021, day #02 problem 1
// Day 2: Dive!
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2021-12-29

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

/* 
    Assuing here commands to submarine are of form "<cmd> <distance>", where
        <cmd> is one-word text command (forward, down, up)
        <distance> is integer as argument to the command
 */

int locationHor = 0, locationVer = 0;       // Position on hor/ver coordinates
// forward adjusts the Horizontal location
// down adds to Vertical location, up reduces it

string cmd;                                 // Movement command from input 
int dist = 0;                               // Argument to movement command

int main() {
    cin >> cmd;
    while (cin) {
        cin >> dist;                    // No error handling on inputs
        transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower);
        if (cmd == "forward")               // could also have been formatted as 3 if stetatements
            locationHor += dist;
        else if (cmd == "down") 
            locationVer += dist;
        else if (cmd == "up") 
            locationVer -= dist;
        cout
            << "Command: " << cmd << " " << dist 
            << ", Location (hor,ver) = (" << locationHor << "," << locationVer << ")" << endl;
        cin >> cmd;
    }
    cout 
        << "================" << endl 
        << "Final location(hor,ver) = (" << locationHor << "," << locationVer << ")" 
        << " multiplied = " << locationHor*locationVer << endl;
}