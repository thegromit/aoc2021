// Solution at the Advent Of Code 2021, day 4 problem 1
// Day 4: Giant Squid
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-09..22


#include <vector>
#include <sstream>
#include "bgrid.h"

// bcell.cc here
//=====================

// Constructors
BCell::BCell()  {
    theValue = -1;
    hitValue = 0;
}

BCell::BCell(int value)  {
    theValue = value;
    hitValue = 0;
}

// About value
void BCell::setValue(int value)  {
    theValue = value;
}

int BCell::getValue()  {
    return theValue;
}

// About being hit
bool BCell::isHit()  {
    return hitValue;
}

void BCell::markHit()  {
    hitValue = true;
}

void BCell::clearHit()  {
    hitValue = false;
}

// End of bcell.cc
//=============================

// Class BGrid - 5x5 Bingo grid of BCell instances
//=============================
// BGrid implementation

BGrid::BGrid()  {                                   // Initialize something as empty / zero
    bingoRow = -1;                                  // No hit yet
    bingoCol = -1;
}

BGrid::BGrid(istream& input)  {                     // Read grid from istream
    bingoRow = -1;                                  // No hit yet
    bingoCol = -1;
    int val = -1;
    // Read from stream
    for (int i=0; i<5; i++)  {
        for (int j=0; j<5; j++)  {
            if (!input)  {
/*                 cerr << "BGrid::BGrid(istream&): Input stream closed before finished reading the grid." << endl;
                exit(1);
 */         
                return;                                         // No input error handling outher than return
            }
            input >> val;
            theGrid[i][j].setValue(val);
        }
    } 
}

// Marking
bool BGrid::checkAndMark(int num)  {                            // Check a number and tell if there is a hit
    bool wasHit = false;
    // Go through theGrid and if there is a hit with one of the 5x5 values, mark it as being hit
    for (int i=0; i<5; i++)  {
        for (int j=0; j<5; j++)  {
            if (theGrid[i][j].getValue() == num)  {
                theGrid[i][j].markHit();
                wasHit=true;
            }
        }
    } 
    return wasHit;
}

// Bingo?
bool BGrid::isBingo()  {                                // Will stop on first hit
    // Go through column per column
    for (int j=0; j<5; j++)  {
        if (theGrid[0][j].isHit() && theGrid[1][j].isHit() && theGrid[2][j].isHit() && 
            theGrid[3][j].isHit() && theGrid[4][j].isHit())  {
                bingoCol=j;                                         // Save the column number
                return true;
        }
    } 
    // Go through row per row
    for (int i=0; i<5; i++)  {
        if (theGrid[i][0].isHit() && theGrid[i][1].isHit() && theGrid[i][2].isHit() && 
            theGrid[i][3].isHit() && theGrid[i][4].isHit())  {
                bingoRow=i;                                         // Save the row number
                return true;
        }
    } 
    return false;
}

// Cell access: Getters
BCell& BGrid::getCell(int x, int y)  {
    return theGrid[x][y];
}

int BGrid::getValue(int x, int y)  {
    return theGrid[x][y].getValue();
}

// Cell access: Setters
void BGrid::setCell(int x, int y, BCell cell)  {
    theGrid[x][y] = cell;
}

void BGrid::setValue(int x, int y, int val)  {
    theGrid[x][y].setValue(val);
}

int BGrid::getBingoRow()  {                                  // Row of the achieved Bingo or -1 if no row has bingo
    return bingoRow;
}

int BGrid::getBingoCol()  {                                  // Column of the achieved Bingo or -1 if no Column has bingo
    return bingoCol;
}

// End of BGrid implementation
//===============================


// Report the status of a BGrid object
void reportBGrid(BGrid& grid, int lastHit)  {
    int unmarkedSum = 0;
    if (grid.isBingo())  
        if (grid.getBingoCol() != -1)
            cout << "Grid HAS Bingo on column " << grid.getBingoCol()+1 << "!" << endl;
        else
            cout << "Grid HAS Bingo on row " << grid.getBingoRow()+1 << "!" << endl;
    else 
        cout << ", and it DOES NOT HAVE Bingo." << endl;
    for (int i=0; i<5; i++)  {
        for (int j=0; j<5; j++)  {
            cout << "(" << i << "," << j << ") = ";
            if (grid.getCell(i, j).isHit()) 
                cout << "_" << grid.getCell(i,j).getValue() << "_  ";
            else  {
                cout << grid.getCell(i,j).getValue() << "  ";
                unmarkedSum += grid.getCell(i,j).getValue();
            }
        }
        cout << endl;
    }
    cout << "Sum of unmarked numbers = " << unmarkedSum << ", and the last number hit = " << lastHit << endl;
    cout << "Multiplied they make " << unmarkedSum * lastHit << endl;
}


// Let's test it out
int main()  {
    // Read random integer series, all on one line, separated by comma
    string input;
    cin >> input;
    cout << input << endl;
    int inputNum = -1;                                  // Input value for one random number in string "input"
    vector<int> inputVal;                               // Collect input integers here

    // Set up string stream and split input into integers
    stringstream inputStream(input);
    while (inputStream >> inputNum)  {
        inputVal.push_back(inputNum);
        if (inputStream.peek() == ',')
            inputStream.ignore();
    }

    // Calling empty grid
/*     BGrid emptyG;
    reportBGrid(emptyG, "emptyG");
 */    // Initializing from stream    
    /* BGrid fromCin1(cin);
    reportBGrid(fromCin1, "fromCin1");
    BGrid fromCin2(cin);
    reportBGrid(fromCin2, "fromCin2");
    BGrid fromCin3(cin);
    reportBGrid(fromCin3, "fromCin3"); */

    // Read grids as many as there are
    vector<BGrid> grids;
    while (cin)  {
        BGrid grid(cin);
        if (cin)  {                                         // Do not store empty grid (past input)
            grids.push_back(grid);
//             reportBGrid(grid, "Read from cin");
        }
    }

    // Report input situation
    cout << inputVal.size() << " random input values read." << endl;
    cout << grids.size() << " grids read." << endl;

    // Work from the input, stop at the 1st bingo
    for (auto& it : inputVal)  {
        for (auto& gr : grids)  {
            if (gr.checkAndMark(it))  {
                if (gr.isBingo())  {
                    cout << "Value " << it << " caused a BINGO in the following grid: " << endl;
                    reportBGrid(gr, it);
                    exit(0);
                }
            }
        }
    }
    // No bingo
    cout << "No Bingo was found." << endl;
}