/*
// Class BCell - Cell in a 5x5 Bingo grid
// 
// Created for the Advent Of Code 2021, day 4 problem 1
// Day 4: Giant Squid
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-09
*/

#include <iostream>
#include <string>

using namespace std;

#include "bcell.h"

// Constructors
BCell::BCell()  {
    theValue = -1;
    hitValue = 0;
}

BCell::BCell(int value)  {
    theValue = value;
    hitValue = 0;
}

// About value
void BCell::setValue(int value)  {
    theValue = value;
}

int BCell::getValue()  {
    return theValue;
}

// About being hit
bool BCell::isHit()  {
    return hitValue;
}

void BCell::markHit()  {
    hitValue = true;
}

void BCell::clearHit()  {
    hitValue = false;
}

#ifdef TEST_IT

// Helper function to report status of a BCell object
void reportBCell(BCell& cell, string name)  {
    cout << "BCell " << name << ", value = " << cell.getValue();
    if (cell.isHit())
        cout << ", and it HAS been marked." << endl;
    else 
        cout << ", and it HAS NOT been marked." << endl;
}


// Let's try out the class
int main()  {
    BCell cell1{42};
    reportBCell(cell1, "cell1{42} constructor");
    cell1.markHit();
    reportBCell(cell1, "cell1 after marking");
    cell1.clearHit();
    reportBCell(cell1, "cell1 after clearing the marking");
    cell1.setValue(27);
    reportBCell(cell1, "cell1 after setting value as 27");
}

#endif