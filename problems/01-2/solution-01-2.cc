// Solution at the Advent Of Code 2021, day #01 problem 2
// Day 1: Sonar Sweep
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2021-12-29

#include <iostream>
#include <string>

using namespace std;

/* 
Sliding window for sum of 3 input values.
Working together with the 01-1 solution will provide 3 sum sliding window for depth measurements:
    solution-01-2 < input-01-1 | solution-01-1 > output-01-2
 */

int main() {
    // We will monitor the sum of these 3 values 
    int val1 = -1, val2 = -1, val3 = -1, 
        input = -1;     // Assume no valid input is "-1"

    // Input values until data ends
    cin >> input;
    while (cin) {
        val3 = input;
        if ((val1 > -1) && (val2 > -1) && (val3 > -1)) {
            // Assume data is OK, calculate & output sum
            cout << val1 + val2 + val3 << endl; 
        }
        val1 = val2;
        val2 = val3;
        cin >> input;
    }
}