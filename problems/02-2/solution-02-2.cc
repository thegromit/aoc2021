// Solution at the Advent Of Code 2021, day #02 problem 2
// Day 2: Dive!
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2021-12-29

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

/* 
    Assuing here commands to submarine are of form "<cmd> <distance>", where
        <cmd> is one-word text command (forward, down, up)
        <dist> is integer as argument to the command
 */

int locationHor = 0, locationVer = 0,       // Position on hor/ver coordinates
    aim = 0;                                // Default at going only forward without diving

/* 
    - "aim" tells how steep dive is taking place
    - command "forward" adjusts the Horizontal location dist units and Vertical for aim*dist units
    - "down" adds to aim dist amount, "up" reduces it
 */

string cmd;                                 // Movement command from input 
int dist = 0;                               // Argument to movement command

int main() {
    cin >> cmd;
    while (cin) {
        cin >> dist;                        // No error handling on inputs...
        transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower); //convert cmd to lower case
        if (cmd == "forward") {               // could also have been formatted as 3 if stetatements
            locationHor += dist;            // Forward for dist
            locationVer += (dist * aim);    // Aim determines how fast we go deeper
        }
        else if (cmd == "down") 
            aim += dist;                    // Adjust angle (aim) to go steeper down
        else if (cmd == "up") 
            aim -= dist;                    // Adjust angle (aim) to go less steep down
        cout
            << "Command: " << cmd << " " << dist 
            << ", Location (hor,ver) = (" << locationHor << "," << locationVer << ")" 
            << " and aim = " << aim << endl;
        cin >> cmd;
    }
    cout 
        << "================" << endl 
        << "Final location(hor,ver) = (" << locationHor << "," << locationVer << ")" 
        << " multiplied = " << locationHor*locationVer << endl;
}