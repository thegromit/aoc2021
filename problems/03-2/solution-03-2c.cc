// Solution at the Advent Of Code 2021, day #03 problem 2
// --- Day 3: Binary Diagnostic ---
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-02

#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>        // Need exp2()
#include <vector>

using namespace std;

/* 
    Diagnostics in binary form, here it tells Oxygen level and CO2 scrubbing rating.
    Assumptions:
    - Each bit string of same length (set by 1st measurement value)
    - Power consumption string is max 20 binary characters long
    Task:
    - Determinate O2 Generator level by iteratively reducing input set bit-by-bit to include the most common 0/1 value 
    - Determinate CO2 Scrubbing level by iteratively reducing input set bit-by-bit to include the least common 0/1 value 
    - Determine Life support rating by mutiplying O2 * CO2 
*/

// Inputs
int diagStrLen = -1;                                            // Length of 1st measurement 
const int MAX_DIAG_LEN = 20;                                    // Max bit string length for power diagnostics

vector<string>  diagInputs,                                     // Storage for all inputs fir later filtering
                O2Inputs,                                       // Copy for determining O2 generator values
                CO2Inputs;                                      // Copy for determining CO2 scrubbing inputs

// Formulating outputs
int O2Val = 0, CO2Val = 0;                                      // Final values


/*  Function findMostCommonBit: Check which bit value is most common on location "loc"
    Inputs:
        inputs: Vector of strings to search through
        loc: location of bit character to search for
    Output:
        0: Zeros were a majority
        1: Ones were a majority
        -1: Equal number of zeros and ones
    Assumptions:
        - All strings in inputs vector are of same length
        - All strings contain only zeros and ones
 */
int findMostCommonBit(int loc, vector<string> &inputs) {
    int zeroCount = 0, oneCount = 0;
    for (auto& it : inputs) {
        if (it[loc] =='0') 
            zeroCount++;
        else
            oneCount++;
    }
    if (zeroCount > oneCount)
        return 0;                                       // 0 won
    if (oneCount > zeroCount)
        return 1;                                       // 1 won
    return -1;                                          // Equal 0s and 1s 
}

/*  Function filterVector: Clean up vector from strings not fitting the chosen bit value
    Inputs:
        loc: location of bit character to search for
        val: Bitvalue on location loc that is the filtering criteria
        inputs: Vector of strings to search through
 */
/* void filterVector(int loc, int val, vector<string> &inputs) {
    // None for now
}
 */

///////////////////////////////////////////////
int main() {
    string diagStr;                                             // Measurement data in string format
    cin >> diagStr;                                             // Initial read
    // Next read all input int a vector
    while (cin) {
        cout << diagStr; 
        // Check diagnostics string length
        if (diagStrLen == -1) {
            diagStrLen = diagStr.length();                    // Capture power diagnostics bit string length
            cout << ", Assuming the string length will be from here on " << diagStrLen;
        }
        cout << endl;
        if (diagStr.length() != diagStrLen) {                   // Fail on invalid input length
            cerr 
                << "Inconsistent length for diagnostics input, 1st length = " << diagStrLen
                << " and later input length was " << diagStr.length();
            exit(1);
        }
        // Store diagnostics data string into a vector that hosts all inputs
        diagInputs.push_back(diagStr);
        cin >> diagStr;                                         // Get next input
    }
    // Report input amount
    cout 
        << "================" << endl << diagInputs.size() 
        << " diagnostic measurements" << endl << endl;

    // Make separate copies of input for O2 and CO2 measurement analysis
    O2Inputs = diagInputs;
    CO2Inputs = diagInputs;

    // Find out the O2 Generator status
    int mostCommon;
    for (int i=0; i<diagStrLen; i++) {
        mostCommon = findMostCommonBit(i, O2Inputs);
        cout << "Bit #" << i << ": " << mostCommon;
        if (mostCommon == -1)
            mostCommon = 1;
        // Now clean away wrong values
        cout << ", and inputs shrink from " << O2Inputs.size();
        // filterVector(i, mostCommon, O2Inputs);
        cout << " to " << O2Inputs.size() << " values" << endl;
    }
}