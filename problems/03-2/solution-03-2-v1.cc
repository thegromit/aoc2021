// Solution at the Advent Of Code 2021, day #03 problem 2
// --- Day 3: Binary Diagnostic ---
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-02

#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>        // Need exp2()
#include <vector>

using namespace std;

/* 
    Diagnostics in binary form, here it tells Oxygen level and CO2 scrubbing rating.
    Assumptions:
    - Each bit string of same length (set by 1st measurement value)
    - Power consumption string is max 20 binary characters long
    Task:
    - Determinate O2 Generator level by iteratively reducing input set bit-by-bit to include the most common 0/1 value 
    - Determinate CO2 Scrubbing level by iteratively reducing input set bit-by-bit to include the least common 0/1 value 
    - Determine Life support rating by mutiplying O2 * CO2 
*/

// Inputs
string diagStr;                                                 // Measurement data in string format
int diagStrLen = -1;                                            // Length of 1st measurement 
const int MAX_DIAG_LEN = 20;                                    // Max bit string length for power diagnostics

vector<string>  diagInputs,                                     // Storage for all inputs fir later filtering
                O2Inputs,                                       // Copy for determining O2 generator values
                CO2Inputs;                                      // Copy for determining CO2 scrubbing inputs

// Formulating outputs
int O2Val = 0, CO2Val = 0;                                      // Final values


/*  
    Check which bit value is most common on location "loc", 
    and filter out all values from "inputs" vector that have 
    less popular value in location "loc".
 */
void findMostCommonBitAndFilter(int loc, vector<string> &inputs) {
    int zeroCount = 0, oneCount = 0;
    char chosen;
    vector<string>::iterator iter;                              // Iterator for vector passing
    string s;
    if (loc < 0 || loc >= diagStrLen) {
        cerr 
            << "Inconsistent location for the diagnostics input, 1st length = " << diagStrLen
            << " and required location was " << loc << endl;
        exit(1);
    }
    int origSize = inputs.size();                               // Size before filtering
    if (origSize == 1) 
        return;                                                 // Already found the value
    for (iter = inputs.begin(); iter != inputs.end(); iter++) {
        s = *iter;
        if (s[loc] =='0') zeroCount++;
        else if (s[loc] =='1') oneCount++;
        else {
            cerr 
                << "Invalid content in input: " << s  
                << ", should be only 0 or 1" << endl;
            exit(1);
        }
    }
    if (zeroCount > oneCount) chosen = '0';
    else chosen = '1';                                          // Bias for '1'
    // Now remove all where character in location "loc" is not the "chosen"
    for (iter = inputs.begin(); iter != inputs.end(); iter++) {
        s = *iter;
        if (s[loc] != chosen) {
            cout << "Loc " << loc <<": Removing measurement " << s  
                << "for missing (" << chosen << ")" << endl;
            iter = inputs.erase(iter)--;                        // .erase() returns pointer to next item
        }
    }    
}

int main() {
    cin >> diagStr;                                             // Initial read
    // Next read all input int a vector
    while (cin) {
        cout << diagStr; 
        // Check diagnostics string length
        if (diagStrLen == -1) {
            diagStrLen = diagStr.length();                    // Capture power diagnostics bit string length
            cout << ", Assuming the string length will be from here on " << diagStrLen;
        }
        cout << endl;
        if (diagStr.length() != diagStrLen) {                   // Fail on invalid input length
            cerr 
                << "Inconsistent length for diagnostics input, 1st length = " << diagStrLen
                << " and later input length was " << diagStr.length();
            exit(1);
        }
        // Store diagnostics data string into a vector that hosts all inputs
        diagInputs.push_back(diagStr);
        cin >> diagStr;                                         // Get next input
    }
    // Report input amount
    cout 
        << "================" << endl << diagInputs.size() 
        << " diagnostic measurements" << endl << endl;

    // Make separate copies of input for O2 and CO2 measurement analysis
    O2Inputs = diagInputs;
    CO2Inputs = diagInputs;

    // Find out the O2 Generator status
    for (int i=0; i<diagStrLen; i++) {
        findMostCommonBitAndFilter(i, O2Inputs);
        if (O2Inputs.size() == 1) break;                  // No need to filter further if we have only 1 input easurement left
    }
    if (O2Inputs.size() != 1) {
        cerr 
            << "O2 Generator status data inconclusive, " 
            << O2Inputs.size() << " values left after filtering." << endl;
        exit(1);
    }
    O2Val = stoi(O2Inputs[0], nullptr, 2);
    if (O2Val <= 0) {
        cerr 
            << "O2 Generator status data conversion failed: " 
            << O2Inputs[0] << " interpreted as " << O2Val << endl;
        exit(1);
    }


    // Inputs filtered and values collected , so let's formulate final report
    cout << "================" << endl;
    cout 
        << "O2Val = " << O2Val << ", CO2Val = " << CO2Val 
        << ", multiplied = " << O2Val * CO2Val << endl;
}