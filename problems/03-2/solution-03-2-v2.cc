// Solution at the Advent Of Code 2021, day #03 problem 2
// --- Day 3: Binary Diagnostic ---
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-02

#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>        // Need exp2()
#include <vector>

using namespace std;

/* 
    Diagnostics in binary form, here it tells Oxygen level and CO2 scrubbing rating.
    Assumptions:
    - Each bit string of same length (set by 1st measurement value)
    - Power consumption string is max 20 binary characters long
    Task:
    - Determinate O2 Generator level by iteratively reducing input set bit-by-bit to include the most common 0/1 value 
    - Determinate CO2 Scrubbing level by iteratively reducing input set bit-by-bit to include the least common 0/1 value 
    - Determine Life support rating by mutiplying O2 * CO2 
*/

// Inputs
int diagStrLen = -1;                                            // Length of 1st measurement 
const int MAX_DIAG_LEN = 20;                                    // Max bit string length for power diagnostics

vector<string>  diagInputs,                                     // Storage for all inputs fir later filtering
                O2Inputs,                                       // Copy for determining O2 generator values
                CO2Inputs;                                      // Copy for determining CO2 scrubbing inputs


/*  Function findMostCommonBit: Check which bit value is most common on location "loc"
    Inputs:
        inputs: Vector of strings to search through
        loc: location of bit character to search for
    Output:
        0: Zeros were a majority
        1: Ones were a majority
        -1: Equal number of zeros and ones
    Assumptions:
        - All strings in inputs vector are of same length
        - All strings contain only zeros and ones
 */
int findMostCommonBit(int loc, vector<string> &inputs) {
    int zeroCount = 0, oneCount = 0;
    for (auto& it : inputs) {
        if (it[loc] =='0') 
            zeroCount++;
        else
            oneCount++;
    }
    if (zeroCount > oneCount)
        return 0;                                       // 0 won
    if (oneCount > zeroCount)
        return 1;                                       // 1 won
    return -1;                                          // Equal 0s and 1s 
}

/*  Function filterVector: Clean up vector from strings not fitting the chosen bit value
    Inputs:
        loc: location of bit character to search for
        val: Bitvalue on location loc that is the filtering criteria
        inputs: Vector of strings to search through
 */
void filterVector(int loc, char val, vector<string> &inputs) {
    string s;
    // DIAG
/*     cout << "Filtering for " << val << " in vector of " << inputs.size() << " elements." << endl;
 */    // for (iter = inputs.begin(); iter != inputs.end(); iter++) {
    int inputsSize = inputs.size();
    for (int i = 0; i < inputsSize; i++) {
        s = inputs[i];
        // DIAG
/*         cout << "Loc " << loc <<", i = " << i << ", size = " << inputs.size() << ": ";
 */        if (s[loc] != val) {
            // DIAG
/*             cout << "Removing measurement " << s   
                << " for missing (" << val << ")" << endl;
 */            inputs.erase(inputs.begin()+i);
            i--;                                                // inputs[i] was reomoved, so next item will recide on same index
            inputsSize = inputs.size();
        } else {
            // DIAG
/*             cout << "String " << s << " is OK" << endl;
 */        }
    }    
}


///////////////////////////////////////////////
int main() {
    string diagStr;                                             // Measurement data in string format
    cin >> diagStr;                                             // Initial read
    // Next read all input int a vector
    while (cin) {
        cout << diagStr; 
        // Check diagnostics string length
        if (diagStrLen == -1) {
            diagStrLen = diagStr.length();                    // Capture power diagnostics bit string length
            cout << ", Assuming the string length will be from here on " << diagStrLen;
        }
        cout << endl;
        if (diagStr.length() != diagStrLen) {                   // Fail on invalid input length
            cerr 
                << "Inconsistent length for diagnostics input, 1st length = " << diagStrLen
                << " and later input length was " << diagStr.length();
            exit(1);
        }
        // Store diagnostics data string into a vector that hosts all inputs
        diagInputs.push_back(diagStr);
        cin >> diagStr;                                         // Get next input
    }
    // Report input amount
    cout 
        << "================" << endl << diagInputs.size() 
        << " diagnostic measurements" << endl;

    // Make separate copies of input for O2 and CO2 measurement analysis
    O2Inputs = diagInputs;
    CO2Inputs = diagInputs;

    int mostCommon;
    char lookFor = 'X';
    int sizePre = 0;

    // Find out the O2 Generator status
    cout << endl << "Find out the O2 Generator status" << endl;
    for (int i=0; i<diagStrLen; i++) {
        mostCommon = findMostCommonBit(i, O2Inputs);
        // Convert int to char
        if (mostCommon == 0)
            lookFor = '0';                              // O2 data found on most commong bits
        else if ((mostCommon == 1) || (mostCommon == -1))
            lookFor = '1';
        cout << "Bit #" << i << ": " << lookFor;
        // Now clean away wrong values
        sizePre = O2Inputs.size();
        filterVector(i, lookFor, O2Inputs);
        cout << ", and inputs was shrank from " << sizePre;
        cout << " to " << O2Inputs.size() << " values." << endl;
        // Check whether we converged at one value already
        if (O2Inputs.size() == 1)
            break;
    }
    cout << " O2 Generator: The chosen string = " << O2Inputs[0] << endl;

    // Find out the CO2 Scrubbing status
    cout << endl << "Find out the CO2 Scrubbing status" << endl;
    for (int i=0; i<diagStrLen; i++) {
        mostCommon = findMostCommonBit(i, CO2Inputs);
        // Convert int to char
        if (mostCommon == 0)
            lookFor = '1';                              // CO2 data found on leat commong bits
        else if ((mostCommon == 1) || (mostCommon == -1))
            lookFor = '0';
        cout << "Bit #" << i << ": " << lookFor;
        // Now clean away wrong values
        sizePre = CO2Inputs.size();
        filterVector(i, lookFor, CO2Inputs);
        cout << ", and inputs was shrank from " << sizePre;
        cout << " to " << CO2Inputs.size() << " values." << endl;
        // Check whether we converged at one value already
        if (CO2Inputs.size() == 1)
            break;

    }
    cout << " CO2 Generator: The chosen string = " << CO2Inputs[0] << endl;

    // Input ran out, so let's formulate gamma and epsilon
    // Formulating outputs
    int O2Val = 0, CO2Val = 0;                                      // Final values
    string O2s = O2Inputs[0], CO2s = CO2Inputs[0];
    for (int i=0; i<diagStrLen; i++) {    
        if (O2s[i] == '1')
            O2Val += exp2(diagStrLen-i-1);
        if (CO2s[i] == '1')
            CO2Val += exp2(diagStrLen-i-1);
    }
    // Final report
    cout 
        << "================" << endl 
        << "O2 value = " << O2Val 
        << ", CO2 value = " << CO2Val
        << ", multiplied = " << O2Val * CO2Val << endl;
}
