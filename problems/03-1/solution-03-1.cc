// Solution at the Advent Of Code 2021, day #03 problem 1
// --- Day 3: Binary Diagnostic ---
// https://adventofcode.com
//
// Erkki A. Pöyhönen, eap@iki.fi, @EkiAP
// 2022-01-02

#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>        // Need exp2()

using namespace std;

/* 
    Diagnostics in binary form, each bit meaning different thing. Here it is power consumption.
    Assumptions:
    - Each bit string of same length (set by 1st measurement value)
    - Power consumption string is max 20 binary characters long
    Task:
    - Calculate "Gamma" by finding out whether 1 or 0 is most common on each position and convert to number
    - Calculate "Epison" by finding out whether 1 or 0 is least common on each position and convet to number
    - Multiply Gamma * Epsilon
 */

// Inputs
string powerStr;                                                // Measurement in string format
int powerStrLen = -1;                                           // Length of 1st measurement 
const int MAX_POWER_LEN = 20;                                   // Max bit string length for power diagnostics

// Analyzing inputs
int zeroCounters[MAX_POWER_LEN], oneCounters[MAX_POWER_LEN];      // Counting appearance of zeros and ones in diag strings

// Formulating outputs
int gammaVal = 0, epsilonVal = 0;                                     // Final values


int main() {
    // Initialize bit counters
    for (int i=0; i<MAX_POWER_LEN; i++) {
        zeroCounters[i] = 0;
        oneCounters[i]= 0;
    }

    cin >> powerStr;                                            // Initial read
    while (cin) {
        cout << powerStr; 
        if (powerStrLen == -1) {
            powerStrLen = powerStr.length();                    // Capture power diagnostics bit string length
            cout << ", Assuming the string length will be from here on " << powerStrLen;
        }
        cout << endl;
        if (powerStr.length() != powerStrLen) {                 // Fail on invalid input length
            cerr 
                << "Inconsistent length for diagnostics input, 1st length = " << powerStrLen
                << " and later input length was " << powerStr.length();
            exit(1);
        }
        // Calculating zeros and ones
        for (int i=0; i<powerStrLen; i++) {
            if (powerStr[i] == '0') 
                zeroCounters[i]++;
            else if (powerStr[i] == '1') 
                oneCounters[i]++;
            else {                                              // Invaid character in supposedly bit string 
                cerr << "Invalid input: " << powerStr;
                exit(1);
            }
        }
        cin >> powerStr;                                        // Next power diagnostics message
    }
    // Print out counters
    cout << "Zero counters: ";
    for (int i=0; i<powerStrLen; i++) {
        cout << zeroCounters[i] << " ";
    }
    cout << endl;
    cout << "One counters: ";
    for (int i=0; i<powerStrLen; i++) {
        cout << oneCounters[i] << " ";
    }
    cout << endl;
    // Input ran out, so let's formulate gamma and epsilon
    cout << "================" << endl;
    for (int i=0; i<powerStrLen; i++) {
        if (zeroCounters[i] > oneCounters[i])                   // gamma is one on a tie, epsilon 0 on a tie
            gammaVal += exp2(powerStrLen-i-1);
        else 
            epsilonVal += exp2(powerStrLen-i-1);
        cout << "Gamma = " << gammaVal << ", Epsilon = " << epsilonVal << endl;
    }
    // Final report
    cout 
        << "================" << endl 
        << "Gamma = " << gammaVal 
        << ", Epsilon = " << epsilonVal
        << ", multiplied = " << gammaVal * epsilonVal << endl;
}